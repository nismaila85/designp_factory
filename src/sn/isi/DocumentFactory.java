package sn.isi;


public class DocumentFactory {
    public Document createdocument(String choix)
    {
        if (choix == null || choix.isEmpty())
            return null;
        switch (choix) {
        case "PDF":
            return new PDF();
        case "TEXTE":
            return new TEXTE();
        default:
            throw new IllegalArgumentException("Unknown choix "+choix);
        }
    }
}